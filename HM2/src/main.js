import func1 from './js_file1';
import func2 from './js_file_2';
import funcTs from './ts_file';
import template from './template.html';

console.log('main.js output');

func1();
func2();
funcTs();

class User {
  constructor(name) {
    this.name = name;
  }
  sayHi() {
    alert(this.name);
  }
}

document.getElementById('name').innerHTML = template({
  name: 'Andrew'
});