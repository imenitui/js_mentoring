const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {
  const isProduction = env.NODE_ENV === 'production' ? true : false;

  return {
    mode: env.NODE_ENV,
    entry: {
      app: ['./src/main.js', './src/less_style.less'],
      vendor: ['jquery', 'lodash']
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './dist'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all'
                }
            }
        }
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          use: 'html-es6-template-loader'
        },
        {
          test: /\.ts?$/,
          exclude: /node_modules/,
          use: [
            'ts-loader',
            'eslint-loader'
          ]
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: 'es2015'
              }
            },
            'eslint-loader'
          ],

        },
        {
          test: /\.css$/,
          exclude: /node_modules/,
          use: [
            MiniCssExtractPlugin.loader,
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                minimize: isProduction,
                importLoaders: 1
              }
            },
            'postcss-loader'
          ]
        },
        {
          test: /\.less$/,
          exclude: /node_modules/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                minimize: isProduction,
                importLoaders: 1
              }
            },
            'postcss-loader',
            'less-loader'
          ]
        }
      ]
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js']
    },
    stats: {
      children: false
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new MiniCssExtractPlugin({ filename: '[name].css'}),
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
    ]
  }
};

// --env.NODE_ENV=development 